<?php
/**
 * This file adds the Home Page to the AgentPress Pro Theme.
 *
 * @author StudioPress
 * @package AgentPress Pro
 * @subpackage Customizations
 */

//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'agentpress_front_page_enqueue_scripts' );
function agentpress_front_page_enqueue_scripts() {

  //* Load scripts only if custom background is being used
  if ( ! get_option( 'agentpress-home-image' ) )
    return;

  //* Enqueue Backstretch scripts
  wp_enqueue_script( 'agentpress-backstretch', get_bloginfo( 'stylesheet_directory' ) . '/js/backstretch.js', array( 'jquery' ), '1.0.0' );
  wp_enqueue_script( 'agentpress-backstretch-set', get_bloginfo( 'stylesheet_directory' ).'/js/backstretch-set.js' , array( 'jquery', 'agentpress-backstretch' ), '1.0.0' );

  wp_localize_script( 'agentpress-backstretch-set', 'BackStretchImg', array( 'src' => str_replace( 'http:', '', get_option( 'agentpress-home-image' ) ) ) );

  //* Add agentpress-pro-home body class
  add_filter( 'body_class', 'agentpress_body_class' );

}

add_action( 'genesis_meta', 'agentpress_home_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function agentpress_home_genesis_meta() {

  if ( is_active_sidebar( 'home-featured' ) || is_active_sidebar( 'home-top' ) || is_active_sidebar( 'home-middle-1' ) || is_active_sidebar( 'home-middle-2' ) || is_active_sidebar( 'home-middle-3' ) || is_active_sidebar( 'home-bottom' ) ) {

    //* Force full-width-content layout setting
    add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

    //* Remove breadcrumbs
    remove_action( 'genesis_before_content_sidebar_wrap', 'genesis_do_breadcrumbs' );

    //* Remove the default Genesis loop
    remove_action( 'genesis_loop', 'genesis_do_loop' );

    //* Add home featured area
    add_action( 'genesis_after_header', 'agentpress_home_featured_widget' );

    //* Add home widget area
    add_action( 'genesis_before_footer', 'agentpress_home_widgets', 1 );

  }
}

function agentpress_body_class( $classes ) {

  $classes[] = 'agentpress-pro-home';
  return $classes;

}

function agentpress_home_featured_widget() {

  genesis_widget_area( 'home-featured', array(
    'before' => '<div class="home-featured full-width widget-area"><div class="wrap">',
      'after' => '</div></div>',
  ) );

}

function agentpress_home_widgets() {

  genesis_widget_area( 'home-top', array(
    'before' => '<div class="home-top full-width widget-area"><div class="wrap">',
      'after'  => '</div></div>',
  ) );

  $pages = array("attic", "basement", "bathroom", "bedroom", "ceilings", "closet");

  echo '<div class="home-top full-width widget-area">';
  echo '<div class="wrap">';
  echo '<section id="featured-listings-3" class="featured-pages full-width widget featured-listings">';
  echo '<div class="widget-wrap">';

  $index = 0;
  
  foreach ($pages as $page) {
    $p = get_page_by_path(html_entity_decode($page), OBJECT);
    $pic = get_the_post_thumbnail(html_entity_decode($p->ID), 'properties');
    $link = get_permalink(html_entity_decode($p->ID));
    if ($index % 2 == 0) {
      $cl = "left";
    } else {
      $cl = "right";
    }
    $index += 1;
    
    echo '<div class="' . $cl .' post-500 listing type-listing status-publish has-post-thumbnail types-single-family-home bedrooms-2-bedrooms price-500k-750k entry">';
    echo '<div class="widget-wrap">';
    echo '<div class="listing-wrap">';
    echo '<a href="' . $link  . '">';
    echo $pic;
    echo '</a>';
    echo '<span class="listing-price">';
    echo $p->post_title;
    echo '</span>';
    echo '<span class="listing-address">';
    echo "filler text";
    echo '</span>';
    echo '</div> </div> </div>';
  }
  echo '</div>';
  echo '</section>';
  echo '</div> </div>';

  if ( is_active_sidebar( 'home-middle-1' ) || is_active_sidebar( 'home-middle-2' ) || is_active_sidebar( 'home-middle-3' ) ) {

    echo '<div class="home-middle"><div class="wrap">';
  
    genesis_widget_area( 'home-middle-1', array(
      'before' => '<div class="home-middle-1 full-width widget-area">',
	'after'  => '</div>',
    ) );
  
    genesis_widget_area( 'home-middle-2', array(
      'before' => '<div class="home-middle-2 widget-area">',
	'after'  => '</div>',
    ) );
  
    genesis_widget_area( 'home-middle-3', array(
      'before' => '<div class="home-middle-3 widget-area">',
	'after'  => '</div>',
    ) );
  
    echo '</div></div>';

  }

  genesis_widget_area( 'home-bottom', array(
    'before' => '<div class="home-bottom full-width widget-area"><div class="wrap">',
      'after'  => '</div></div>',
  ) );

}

genesis();
?>
